FROM node:12.4-alpine
LABEL maintainer="Linus Kohl"

RUN mkdir /app
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .

EXPOSE 5000
CMD ["node", "server/main.js"]

