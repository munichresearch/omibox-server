const express = require('express');
const { createServer } = require('http');
const socket = require('./socket');

const app = express();
const server = createServer(app);

app.use('/', express.static(`${process.cwd()}/../client`));

module.exports.run = () => {
  server.listen(5000);
  socket(server);
  console.log(`Server is listening`);
};
